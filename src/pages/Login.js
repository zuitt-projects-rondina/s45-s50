import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'


export default function Login(props){

	// allows us to consume thr User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);


	function logInUser(e){
		e.preventDefault()

		fetch('http://localhost:4000/users/login', {
			method : "POST",
			headers : {
				'Content-Type' : 'application/json'
			},
			body : JSON.stringify({
				email : email,
				password : password
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(typeof data.access !== "undefined"){

				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title : 'Login Successful',
					icon: 'success',
					text: 'Welcome to Zuitt'

				})
			} else {

				Swal.fire({
					title : 'Login Failed',
					icon: 'error',
					text: 'Please check your email and password'
				})

			}

		})



		// set the email of the authenticated user in the local storage

		/*// Syntax : localStorage.setItem("key", value)
		localStorage.setItem("email", email)*/


		// to access the user information, it can be done using localStorage, necessary to update the usersate which will help update the App component and rerender it to avoid refreshing the page upon user login and logout
		/*setUser({
			email : localStorage.getItem('email')
		})*/

		setEmail('')
		setPassword('')

		const retrieveUserDetails = (token) => {
			fetch('http://localhost:4000/users/details',{

				method : "POST",
				headers : {
					Authorization : `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})

		}

		/*alert('Login Successfully')*/
		

	}

	useEffect(() => {

		if(email !== '' && password !== ''){

			setIsActive(true)
		} else {
			setIsActive(false)
		}


	}, [email, password])




	return(
		(user.id !== null) ?

		<Navigate to = "/courses"/>

		:
		<Form onSubmit={e => logInUser(e)}>
			<h1>Login</h1>
			<Form.Group controlId = "userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type = "email"
					placeholder = "Enter email here"
					value ={email}
					onChange ={e => setEmail(e.target.value)}
					required
				/>
		
			</Form.Group>
			<Form.Group controlId = "password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder = "Enter your password"
					value = {password}
					onChange = {e => setPassword(e.target.value)}
					required

				/>
			</Form.Group>
			{ isActive ?

				<Button variant ="primary" type ="submit" id="submitBtn" className ="my-3">
								Login
				</Button>

				:
				<Button variant ="danger" type ="submit" id="submitBtn" className ="my-3" disabled>
								Login
				</Button>

			}

		</Form>

		)


}