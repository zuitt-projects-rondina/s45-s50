const coursesData = [
	{
		id : "wdc001",
		name : "PHP-Laravel",
		description : "Laravel is primarily used for building custom web apps using PHP. It's a web framework that handles many things that are annoying to build yourself, such as routing, templating HTML, and authentication", 
		price : 45000,
		onOffer : true
	},
	{
		id : "wdc002",
		name : "Python-Django",
		description : "Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers", 
		price : 50000,
		onOffer : true
	},
	{
		id : "wdc003",
		name : "Java-Springboot",
		description : "The Spring Framework is an open-source framework for building enterprise Java applications. Spring aims to simplify the complex and cumbersome enterprise Java application development process by offering a framework that includes technologies such as: Aspect-oriented programming (AOP) Dependency injection", 
		price : 55000,
		onOffer : true
	}
	
	
];

export default coursesData;