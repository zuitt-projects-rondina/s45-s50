import React from 'react'


// create a context object
	//context object is a data type of an object that can be used to store information that cam ben shared to other components within the app
	//it is a different approach to passing information between components and allow us for easier access by avoiding the use of prop drilling

const UserContext = React.createContext();

//the provider component allows orher components to consume the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;

