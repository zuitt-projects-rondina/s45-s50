import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom'
import { Row, Col, Card, Button} from 'react-bootstrap'


export default function CourseCard({courseProp}){

	/*console.log({courseProp})*/

//============ ACTIVITY ================
	

	/*const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);
	

	function enroll(){
		
		if(seats > 0) {
			setCount(count + 1)
			setSeats(seats - 1)

		} else {
			alert('No more seats')
		}


	};


	//Syntax: useEffect(() => {}, [optional parameter])
	useEffect(() => {
			if(seats === 0) {
				alert("No more seats available")
			}


	}, [seats])
*/

	const {name, description, price, _id} = courseProp

	return (

		<Row className= "mt-3 mb-3">
			
			<Col>
					<Card className="cardCourse p-3">
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price</Card.Subtitle>
							<Card.Text>{price}</Card.Text>
							
							<Button variant="primary" as={Link} to={`/courses/${_id}`}>See Details </Button>
							
							
						</Card.Body>
					</Card>

				</Col>

		



		</Row>
	)

}
