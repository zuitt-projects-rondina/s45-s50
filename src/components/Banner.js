import {Row, Col, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'


export default function Banner(){
	return(

		<Row>
			<Col className = "p-5">
				<h1>Zuitt Coding Bootcamp</h1>	
				<p>Oppurtunities for everyone</p>
				<Button as={Link} to="/courses" variant="primary">Enroll Now</Button>
			</Col>

		</Row>


		)

}